# Excel

## Build Setup

```bash
# run dev server
$ yarn dev

# build the project in development mode
$ yarn build:dev

# build the project in production mode
$ yarn build:prod

# run eslint to automate fix errors
$ yarn lintfix
```
