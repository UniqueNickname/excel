import { Formula, Header, Table, Toolbar } from '@/components'

export type Component = Formula | Header | Toolbar | Table
