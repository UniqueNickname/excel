import { emptyArray } from '@/utils'

const CODES = {
    A: 65,
    Z: 90,
}

const createCell = (count: number) => {
    return emptyArray(count)
        .map(
            (_, index) =>
                `<div class="excel-table__cell" contenteditable data-col="${index}"></div>`
        )
        .join('')
}

const createColumn = (tag: string, index: number) => {
    return `
        <div class="excel-table__column" data-type="resizable" data-col="${index}">
            ${tag}
            <div class="excel-table__column-resize" data-resize="col"></div>
        </div>
    `
}

const createRow = (info: string, cells: string) => {
    const resize = info
        ? '<div class="excel-table__row-resize" data-resize="row"></div>'
        : ''
    const data = info ? 'data-type="resizable"' : ''

    return `
        <div class="excel-table__row" ${data}>
            <div class="excel-table__row-info">
                ${info}
                ${resize}
            </div>
            <div class="excel-table__row-data">${cells}</div>
        </div>
    `
}

const toChar = (_: any, index: number) => String.fromCharCode(CODES.A + index)

export const createTable = (rowsCount: number = 15) => {
    const columnsCount = CODES.Z - CODES.A + 1

    const columns = emptyArray(columnsCount)
        .map(toChar)
        .map(createColumn)
        .join('')

    const rows = emptyArray(rowsCount).map((_: any, index: number) =>
        createRow((index + 1).toString(), createCell(columnsCount))
    )

    rows.unshift(createRow('', columns))

    return rows.join('')
}
