import { $, DomType } from '@/core'
import { Component } from '@/interfaces'

type ClassComponents = any[]
interface ExcelOptions {
    components?: ClassComponents
}

export class Excel {
    readonly className: string = 'excel'
    private $el: DomType
    private ClassComponents: ClassComponents
    private components: Component[] = []
    constructor(selector: string, options: ExcelOptions) {
        const $el: DomType = $(selector)
        if ($el) this.$el = $el
        else throw new Error('No element with the selector')
        this.ClassComponents = options.components || []
    }

    getRoot() {
        const $root = $.create('div', this.className)

        this.components = this.ClassComponents.map(Component => {
            const $el = $.create('div', Component.className)
            const component = new Component($el)
            $el.html(component.toHTML())
            $root.append($el)
            return component
        })

        return $root
    }

    render() {
        this.$el.append(this.getRoot())
        this.components.forEach(component => component.init())
    }
}
