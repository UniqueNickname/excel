import { DomListener } from './DomListener'
import { DomType } from '@/core'

interface ExcelComponentOptions {
    name?: string
    listeners?: string[]
}

export class ExcelComponent extends DomListener {
    constructor($root: DomType, options: ExcelComponentOptions = {}) {
        super($root, options.listeners || [], options.name || '')
    }

    toHTML() {
        return ''
    }

    init() {
        this.initDomListeners()
    }

    destroy() {
        this.removeDomListeners()
    }
}
