import { CSS } from '@/interfaces'

class Dom {
    private $el: HTMLElement
    constructor(selector: string | HTMLElement) {
        const $el =
            typeof selector === 'string'
                ? (document.querySelector(selector) as HTMLElement | null)
                : selector
        if ($el) this.$el = $el
        else throw new Error('No element with the selector')
    }

    get data() {
        return this.$el.dataset
    }

    html(html: string | undefined) {
        if (typeof html === 'string') {
            this.$el.innerHTML = html
            return this
        }
        return this.$el.outerHTML.trim()
    }

    findAll(selector: string) {
        const NodeList = this.$el.querySelectorAll(selector)
        const cells: Dom[] = []
        NodeList.forEach(cell => cells.push($(cell as HTMLElement)))
        return cells
    }

    clear() {
        this.html('')
        return this
    }

    append(node: HTMLElement | Dom) {
        if (node instanceof Dom) node = node.$el
        this.$el.appendChild(node)
    }

    closest(selector: string) {
        return $(this.$el.closest(selector) as HTMLElement)
    }

    getCoords() {
        return this.$el.getBoundingClientRect()
    }

    css(styles: CSS) {
        Object.keys(styles)
            .map(styleName => styleName as keyof CSS)
            .forEach(styleName => {
                const newStyle = styles[styleName]
                if (newStyle !== undefined) this.$el.style[styleName] = newStyle
            })
        return this
    }

    getData(key: string) {
        return this.$el.dataset[key]
    }

    on(eventType: string, callback: EventListenerOrEventListenerObject) {
        this.$el.addEventListener(eventType, callback)
    }

    off(eventType: string, callback: EventListenerOrEventListenerObject) {
        this.$el.removeEventListener(eventType, callback)
    }
}

export type DomType = Dom

export function $(selector: string | HTMLElement) {
    return new Dom(selector)
}

$.create = (tagName: string, classes: string = '') => {
    const el: HTMLElement = document.createElement(tagName)
    if (classes) el.classList.add(classes)
    return $(el)
}
