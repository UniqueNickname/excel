import { DomType, ExcelComponent } from '@/core'

export class Header extends ExcelComponent {
    static className: string = 'excel-header'

    constructor($root: DomType) {
        super($root, {
            name: 'Header',
            listeners: ['input', 'click'],
        })
    }

    toHTML() {
        return `
            <input type="text" class="excel-header__title-input" value="New table">
            <div class="excel-header__buttons">
                <button class="excel-header__button">
                    <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor"
                        width="18px" height="18px">
                        <path d="M0 0h24v24H0z" fill="none" />
                        <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
                    </svg>
                </button>
                <button class="excel-header__button">
                    <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor"
                        width="18px" height="18px">
                        <path d="M0 0h24v24H0z" fill="none" />
                        <path
                            d="M10.09 15.59L11.5 17l5-5-5-5-1.41 1.41L12.67 11H3v2h9.67l-2.58 2.59zM19 3H5c-1.11 0-2 .9-2 2v4h2V5h14v14H5v-4H3v4c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z" />
                    </svg>
                </button>
            </div>
        `
    }

    onInput(event: Event) {
        console.log(event)
    }

    onClick(event: Event) {
        console.log(event)
    }
}
