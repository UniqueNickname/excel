import { ResizeData } from '@/interfaces'
import { $, DomType } from '@/core'

export const resize = ($root: DomType, resizeData: ResizeData) => {
    const { $resizer, resizeDirection } = resizeData
    if (resizeDirection) {
        const $parent = $resizer.closest('[data-type="resizable"]')
        const coords = $parent.getCoords()
        const $cells = $root.findAll(`[data-col="${$parent.data.col}"]`)

        let size: number

        document.onmousemove = (mousemove: MouseEvent) => {
            if (resizeDirection === 'col') {
                const delta = mousemove.pageX - coords.right
                size = coords.width + delta
                $resizer.css({ right: -delta + 'px' })
            } else {
                const delta = mousemove.pageY - coords.bottom
                size = coords.height + delta
                $resizer.css({ bottom: -delta + 'px' })
            }
        }

        document.onmouseup = () => {
            document.onmousemove = null
            if (resizeDirection === 'col') {
                $cells.forEach($cell => {
                    $cell.css({ width: size + 'px' })
                })
                $resizer.css({ right: '' })
            } else {
                $parent.css({ height: size + 'px' })
                $resizer.css({ bottom: '' })
            }
        }
    }
}
