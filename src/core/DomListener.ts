import { DomType } from '@/core'
import { capitalize } from '@/utils'

interface IActiveListener {
    type: string
    callback: EventListenerOrEventListenerObject
}

export class DomListener {
    protected $root: DomType
    private listeners: string[]
    private name: string
    private activeListeners: IActiveListener[] = []
    constructor($root: DomType, listeners: string[], name: string) {
        if (!$root) throw new Error('No $root provided for DonListener')
        this.$root = $root
        this.listeners = listeners
        this.name = name
    }

    initDomListeners() {
        this.listeners.forEach(listener => {
            const method = getMethodName(listener)
            if (!(this as any)[method]) {
                throw new Error(
                    `Method ${method} is not implemented in ${this.name} component`
                )
            }
            const EventListener = (this as any)[method].bind(this)
            this.activeListeners.push({
                type: listener,
                callback: EventListener,
            })
            this.$root.on(listener, EventListener)
        })
    }

    removeDomListeners() {
        this.activeListeners.forEach(listener => {
            this.$root.off(listener.type, listener.callback)
        })
        this.activeListeners = []
    }
}

const getMethodName = (eventName: string) => 'on' + capitalize(eventName)
