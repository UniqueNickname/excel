const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const PATHS = {
    src: path.join(__dirname, './src'),
    dist: path.join(__dirname, './dist'),
    assets: 'assets/',
}

const isProd = process.env.NODE_ENV === 'production'
const isDev = !isProd

const filename = ext =>
    isDev
        ? `${PATHS.assets}${ext}/[name].[hash].${ext}`
        : `${PATHS.assets}${ext}/[name].${ext}`

const jsLoaders = ext => {
    const loaders = [
        {
            loader: 'babel-loader',
            options: {
                presets: ['@babel/preset-env'],
                plugins: ['@babel/plugin-proposal-class-properties'],
            },
        },
    ]
    if (ext === 'ts')
        loaders[0].options.presets.push('@babel/preset-typescript')
    if (isDev) loaders.push('eslint-loader')
    if (isProd)
        loaders[0].options.plugins.push([
            'babel-plugin-transform-remove-console',
            { exclude: ['error', 'warn'] },
        ])

    return loaders
}

module.exports = {
    mode: process.env.NODE_ENV,
    target: isDev ? 'web' : 'browserslist',
    devtool: isDev ? 'source-map' : false,
    devServer: {
        contentBase: PATHS.src,
        watchContentBase: true,
        port: 3030,
        hot: isDev,
        overlay: {
            warnings: false,
            errors: true,
        },
    },
    context: PATHS.src,
    entry: {
        app: ['@babel/polyfill', `${PATHS.src}/index.ts`],
    },
    output: {
        filename: filename('js'),
        path: PATHS.dist,
    },
    resolve: {
        extensions: ['.js', '.ts'],
        alias: {
            '@': PATHS.src,
        },
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: '/node_modules/',
                use: jsLoaders('js'),
            },
            {
                test: /\.ts$/,
                exclude: '/node_modules/',
                use: jsLoaders('ts'),
            },
            {
                test: /\.s[ac]ss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
            },
        ],
    },
    plugins: [
        new webpack.SourceMapDevToolPlugin({
            filename: '[file].map',
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: filename('css'),
        }),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: `${PATHS.src}/static/`,
                    to: PATHS.dist,
                },
            ],
        }),
        new HTMLWebpackPlugin({
            template: 'index.html',
            minify: {
                removeComments: isProd,
                collapseWhitespace: isProd,
            },
        }),
    ],
}
