import './assets/scss/index.scss'
import { Header, Toolbar, Formula, Table, Excel } from '@/components'

const excel = new Excel('#app', {
    components: [Header, Toolbar, Formula, Table],
})

excel.render()
