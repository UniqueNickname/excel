import { $, DomType, ExcelComponent } from '@/core'
import { ResizeDirectionType } from '@/interfaces'
import { ResizeData } from '@/interfaces/resize'
import { createTable } from './table.template'
import { resize } from './table.functions'

export class Table extends ExcelComponent {
    static className: string = 'excel-table'

    constructor($root: DomType) {
        super($root, {
            name: 'Table',
            listeners: ['mousedown'],
        })
    }

    toHTML() {
        return createTable(100)
    }

    private getEventTarget(event: Event) {
        const $target: EventTarget | null = event.target
        if ($target instanceof HTMLElement) {
            return $($target)
        }
        return null
    }

    private getResizeData(event: Event): ResizeData | null {
        const $resizer = this.getEventTarget(event)
        if ($resizer) {
            const resizeDirection = $resizer.getData(
                'resize'
            ) as ResizeDirectionType
            if (resizeDirection) return { $resizer, resizeDirection }
            return null
        }
        return null
    }

    onMousedown(mousedown: MouseEvent) {
        const resizeData = this.getResizeData(mousedown)
        if (resizeData) {
            resize(this.$root, resizeData)
        }
    }
}
