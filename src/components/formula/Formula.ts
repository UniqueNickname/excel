import { ExcelComponent } from '@/core'
import { DomType } from '@/core'

export class Formula extends ExcelComponent {
    static className: string = 'excel-formula'

    constructor($root: DomType) {
        super($root, {
            name: 'Formula',
            listeners: ['input'],
        })
    }

    toHTML() {
        return `
            <div class="excel-formula__info">fx</div>
            <div class="excel-formula__input" contenteditable spellcheck="false"></div>
        `
    }

    onInput(event: Event) {
        console.log(event)
    }

    onClick(event: Event) {
        console.log(event)
    }
}
