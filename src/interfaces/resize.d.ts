import { DomType } from '@/core'

export type ResizeDirectionType = 'row' | 'col' | undefined

export interface ResizeData {
    $resizer: DomType
    resizeDirection: ResizeDirectionType
}
