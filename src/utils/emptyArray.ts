export const emptyArray = (size: number) =>
    new Array(size).fill('') as Array<''>
